# TP 02 - Langage C (2) - Liste chaînée
- Implémentation d'une liste chaînée
	- Vidéo d'introduction: [Lien](https://universitelibrebruxelles-my.sharepoint.com/:v:/r/personal/jeremie_roland_ulb_be/Documents/INFOH304/TPs/TP%2002a%20-%20Liste%20chainee%20en%20C.mp4?csf=1&web=1&e=S77OgI)
- Compilation par partie et réutilisation du code
	- Vidéo d'introduction: [Lien](https://universitelibrebruxelles-my.sharepoint.com/:v:/r/personal/jeremie_roland_ulb_be/Documents/INFOH304/TPs/TP%2002b%20-%20Compilation%20par%20parties.mp4?csf=1&web=1&e=LgtXFj)
- Entrée / sorties avec les fichiers
- Inverser "Guerre et Paix"
