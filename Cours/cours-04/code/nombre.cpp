#include <iostream>
using std::cout;
using std::endl;
using std::ostream;

class Entier {
private:
	int valeur;
public:
	Entier( int valeurInitiale = 0) : valeur( valeurInitiale ) {}
	void print ( ostream & out = cout ) const
		{ out << valeur; }
	bool operator< ( const Entier & EntierDroite ) const
		{ return valeur < EntierDroite.valeur; }
	// ...
};

class Reel {
private:
	float valeur;
public:
	Reel( float valeurInitiale = 0) : valeur( valeurInitiale ) {}
	void print ( ostream & out = cout ) const
		{ out << valeur; }
	bool operator< ( const Reel & ReelDroite ) const
		{ return valeur < ReelDroite.valeur; }
	// ...
};

int main() {
	Entier entier (5);
	entier.print(); cout << endl;
	
	Reel reel (3.14);
	reel.print(); cout << endl;
	return 0;
}
