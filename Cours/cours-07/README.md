# Cours 7: Diviser pour régner - Backtracking

- Diviser pour régner
	- Principe du diviser pour régner (divide-and-conquer)
	- Tri par fusion (mergesort)
	- Analyse de complexité
- Techniques de programmation: Backtracking
	- Génération exhaustive
	- Recherche exhaustive
	- Recherche d'une solution
	- Recherche de la meilleure solution
	- Branch-and-bound
